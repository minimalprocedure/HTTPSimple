name := "HTTPSimple"

version := "0.1"

scalaVersion := "2.12.6"

scalacOptions in ThisBuild ++= Seq("-unchecked", "-deprecation")

fork in run := true

val akkaHttpVersion = "10.0.4"

libraryDependencies += "com.typesafe.akka" %% "akka-http" % akkaHttpVersion

