/*
 * Copyright (C) 2018 Massimo Maria Ghisalberti <zairik@gmail.com>
 *
 * The contents of this file are subject to the GNU General Public License
 * Version 3 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.gnu.org/copyleft/gpl.html
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 */

import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import org.pragmas.http._

import scala.collection.immutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

object MainClientTest extends App {

  override
  def main(args: Array[String]): Unit = {

    val client = HTTPSimpleClient()
    val getResponseFuture = client.get(
      "http://127.0.0.1:3000/test",
      //Query.Empty,
      headers = immutable.Seq[HttpHeader](new RawHeader("Accept-Encoding", "gzip, deflate")),
      timeout = 10
    )

    val postResponseFuture = client.post("http://127.0.0.1:3000/test", Query("params" -> "AAAAAA"))

    val postResponseFuture2 = client.post("http://127.0.0.1:3000/test", Query("params" -> "AAAAAA"))

    getResponseFuture.onComplete {
      case Success(res) => {
        println(res)

      }
      case Failure(fail) => {
        sys.error(fail.getMessage)
      }
    }
    postResponseFuture.onComplete {
      case Success(res) => {
        println(res)
      }
      case Failure(fail) => {
        sys.error(fail.getMessage)
      }
    }
    postResponseFuture2.onComplete {
      case Success(res) => {
        println(res)

      }
      case Failure(fail) => {
        sys.error(fail.getMessage)
      }
    }
  }
}

object MainServerTest extends App {
  override def main(args: Array[String]): Unit = {
    val routes1 = List(
      ServerRoute(path = "d/c", contentType = RequestContentType.TEXT(), content = "sono d")
    )
    val server: HTTPSimpleServer = HTTPSimpleServer(HTTPSimpleServerSettings(routes = routes1))

    def postAction(m: Map[String, String]): String = m.toString()

    val routes = List(
      ServerRoute(
        path = "pippo/test.html",
        contentType = RequestContentType.HTML(),
        content = server.loadTextFile("index.html")
      ),
      ServerRoute(
        path = "pippo/image.png",
        contentType = RequestContentType.PNG(),
        content = server.loadBinaryFile("images/image.png")
      ),
      ServerRoute(
        path = "testPrivate",
        contentType = RequestContentType.TEXT(),
        content = "test private",
        method = ServerMethod.Get(),
        isPrivate = true),
      ServerRoute(
        path = "b",
        contentType = RequestContentType.TEXT(),
        method = ServerMethod.Post(),
        action = postAction),
      ServerRoute(
        path = "testEmptyParams",
        contentType = RequestContentType.TEXT(),
        method = ServerMethod.Get(),
        action = postAction)
    )

    server.serve(routes)
  }
}


