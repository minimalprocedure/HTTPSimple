/*
 * Copyright (C) 2018 Massimo Maria Ghisalberti <zairik@gmail.com>
 *
 * The contents of this file are subject to the GNU General Public License
 * Version 3 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.gnu.org/copyleft/gpl.html
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 */

package org.pragmas.http

import java.io.File
import java.nio.file.{Files, Paths}

import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpCharsets, MediaTypes, _}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._

import scala.concurrent.Future

object ServerMethod {

  sealed class M(val name: String, val method: Directive0)

  case class Get() extends M("GET", get)

  case class Post() extends M("POST", post)

  val availableMethods = List[M](Get(), Post())

}

object RequestContentType {

  sealed class CT(val name: String = "", val media: MediaType = MediaTypes.`text/plain`) {
    def asText: MediaType.WithOpenCharset = media.asInstanceOf[MediaType.WithOpenCharset]

    def asBinary: MediaType.Binary = media.asInstanceOf[MediaType.Binary]
  }

  case class HTML() extends CT("HTML", MediaTypes.`text/html`)

  case class TEXT() extends CT("TEXT", MediaTypes.`text/plain`)

  case class CSS() extends CT("CSS", MediaTypes.`text/css`)

  case class JAVASCRIPT() extends CT("JAVASCRIPT", MediaTypes.`application/javascript`)

  case class JPG() extends CT("JPG", MediaTypes.`image/jpeg`)

  case class PNG() extends CT("PNG", MediaTypes.`image/png`)

  case class GIF() extends CT("GIF", MediaTypes.`image/gif`)

}

case class ServerRoute
(
  path: String = "/",
  method: ServerMethod.M = ServerMethod.Get(),
  contentType: RequestContentType.CT = RequestContentType.HTML(),
  content: Any = "Hello",
  isPrivate: Boolean = false,
  action: Map[String, String] => String = null //m => m.toString //"noop"
) {

  def contentAsText = content.asInstanceOf[String]

  def contentAsBinary = content.asInstanceOf[Array[Byte]]

}

case class HTTPSimpleServerSettings
(
  host: String = "localhost",
  port: Int = 8080,
  routes: List[ServerRoute] = Nil,
  public: String = "public",
  privateHosts: List[String] = List("localhost", "127.0.0.1", "0.0.0.0")
)

object HTTPSimpleServer {
  def apply(settings: HTTPSimpleServerSettings): HTTPSimpleServer =
    new HTTPSimpleServer(settings)
}

final class HTTPSimpleServer(settings: HTTPSimpleServerSettings) extends HTTPSimple {

  override val systemName = "HTTPSimpleServer"

  val absoluteStaticFolder: String = {
    val s = new File(settings.public)
    if (s.isAbsolute) settings.public else s.getAbsolutePath
  }
  val indexContent = "index.html"

  private val availableMethodsDirective: Directive0 =
    ServerMethod.availableMethods.foldLeft(get)(_ | _.method)

  private var bindingFuture: Future[Http.ServerBinding] = _

  private val stopPrivateRoute: Route = //{
    host(settings.privateHosts: _*) {
      path("shutdown") {
        get {
          stopServer()
          complete("Server stopping...")
        }
      }
    }

  private val defaultRoutes: Route = {
    getFromDirectory(absoluteStaticFolder) ~
      path("") {
        availableMethodsDirective {
          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, loadTextFile(indexContent)))
          //getFileRoute(indexContent)
        }
      } ~
      path(RemainingPath) { rest =>
        val content =
          if (rest.endsWithSlash) loadTextFile((rest / indexContent).toString())
          else loadTextFile(rest.toString())
        availableMethodsDirective {
          content match {
            case "" => reject
            case _ => complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, content))
          }
        }
      }
  }

  private val creationRoutes = prepareRoutesFromList(settings.routes)

  private def prepareRoutesFromList(routes: List[ServerRoute]): Route = {
    routes.foldLeft[Route](reject) { (a: Route, r) => a ~ makeRouteFrom(r) }
  }

  private def startServer(routes: Route): Future[Http.ServerBinding] = {
    val h = http.getOrElse(start())
    implicit val s = h.system
    log.info("[Going online at http://{}:{}]", settings.host, settings.port)
    bindingFuture = h.bindAndHandle(routes, settings.host, settings.port)
    bindingFuture.failed.foreach { ex =>
      log.error(ex, "[{} Failed to bind to {}:{}]", settings.host, settings.port)
    }
    bindingFuture
  }

  private def stopServer(async: Boolean = true): Unit = {
    bindingFuture
      .flatMap(_.unbind())
      .onComplete { _ =>
        log.info("[Stopping http://{}:{}]", settings.host, settings.port)
        shutdown(async)
      }
  }

  private def resolvePath(fileName: String): String = {
    val file = new File(fileName) match {
      case f if f.isAbsolute => f
      case f if !f.isAbsolute => new File(s"$absoluteStaticFolder/$fileName")
    }
    val fc =
      if (file.isDirectory) new File(s"${file.getAbsolutePath}/$indexContent")
      else file.getAbsoluteFile
    if (fc.exists && fc.canRead) fc.getAbsolutePath
    else ""
  }

  def loadTextFile(fileName: String): String = {
    val fn = resolvePath(fileName)
    if (fn.isEmpty) ""
    else scala.io.Source.fromFile(fn, "UTF-8").getLines.mkString
  }

  def getFileRoute(fileName: String): Route = {
    val fn = resolvePath(fileName)
    if (fn.isEmpty) reject
    else getFromFile(fileName)
  }

  def loadBinaryFile(fileName: String): Array[Byte] = {
    val fn = resolvePath(fileName)
    if (fn.isEmpty) Array[Byte]()
    else Files.readAllBytes(Paths.get(fn))
  }

  private def completeTextReq(mediaType: MediaType.WithOpenCharset, route: ServerRoute, params: Map[String, String] = Map()): StandardRoute = {
    Option(route.action) match {
      case None =>
        complete(HttpEntity(ContentType(mediaType, HttpCharsets.`UTF-8`), route.contentAsText))
      case Some(f) =>
        complete(HttpEntity(ContentType(mediaType, HttpCharsets.`UTF-8`), f(params)))
    }
  }

  private def implPrivateMethod(routeImpl: Route)(implicit route: ServerRoute): Route = {
    if (route.isPrivate) host(settings.privateHosts: _*)(routeImpl)
    else routeImpl
  }

  private def implGetMethod(implicit route: ServerRoute): Route = {
    val m = path(separateOnSlashes(route.path)) {
      route.method.method {
        parameterMap {
          params =>
            if (!route.contentType.media.binary)
              completeTextReq(route.contentType.asText, route, params)
            else
              complete(HttpEntity(ContentType(route.contentType.asBinary), route.contentAsBinary))
        }
      }
    }
    implPrivateMethod(m)
  }

  private def implPostMethod(implicit route: ServerRoute): Route = {
    val m = path(separateOnSlashes(route.path)) {
      route.method.method {
        parameterMap {
          params =>
            formFieldMap {
              fields =>
                completeTextReq(route.contentType.asText, route, params ++ fields)
            }
        }
      }
    }
    implPrivateMethod(m)
  }

  private def makeRouteFrom(route: ServerRoute): Route = {
    route.method match {
      case ServerMethod.Get() => implGetMethod(route)
      case ServerMethod.Post() => implPostMethod(route)
      case _ => reject
    }
  }

  private def loggingRoute(route: Route): Route = {
    val extractReq = extract(_.request)
    extractClientIP { ip =>
      extractReq { req =>
        log.info("[Request: {} {} from {}]",
          req.method,
          req.uri,
          ip.toOption.map(_.getHostAddress).getOrElse("unknown"))
        route
      }
    }
  }

  def serve(routesList: List[ServerRoute] = Nil): Unit = {
    val routes =
      stopPrivateRoute ~
        prepareRoutesFromList(routesList) ~
        creationRoutes ~
        defaultRoutes
    startServer(loggingRoute(routes))
  }

  def stop(async: Boolean = false): Unit = {
    stopServer(async)
  }

}
