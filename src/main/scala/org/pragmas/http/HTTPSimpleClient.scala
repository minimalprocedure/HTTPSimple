/*
 * Copyright (C) 2018 Massimo Maria Ghisalberti <zairik@gmail.com>
 *
 * The contents of this file are subject to the GNU General Public License
 * Version 3 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.gnu.org/copyleft/gpl.html
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 */

package org.pragmas.http

import java.nio._

import akka.http.scaladsl.coding._
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.HttpEncodings
import akka.util.ByteString

import scala.collection.immutable
import scala.concurrent.duration._
import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Success}

case class HTTPSimpleClientResponseMetas
(
  headers: Seq[HttpHeader],
  contentType: ContentType,
  protocol: HttpProtocol,
  statusCode: StatusCode,
)

case class HTTPSimpleClientResponseDataContent
(
  text: Option[String],
  buffer: Option[ByteBuffer]
)

case class HTTPSimpleClientResponseData
(
  metas: HTTPSimpleClientResponseMetas,
  content: HTTPSimpleClientResponseDataContent
)

object HTTPSimpleClient {
  def apply(): HTTPSimpleClient = new HTTPSimpleClient()
}

final class HTTPSimpleClient extends HTTPSimple {

  override val systemName = "HTTPSimpleClient"

  def post(url: String,
           query: Query = Query.Empty,
           headers: immutable.Seq[HttpHeader] = Nil,
           discardEntity: Boolean = false,
           timeout: Int = 0
          ): Future[HTTPSimpleClientResponseData] = {
    doRequest(HttpRequest(method = HttpMethods.POST, uri = url, headers, FormData(query).toEntity), discardEntity, timeout)
  }

  def get(url: String,
          query: Query = Query.Empty,
          headers: immutable.Seq[HttpHeader] = Nil,
          discardEntity: Boolean = false,
          timeout: Int = 0
         ): Future[HTTPSimpleClientResponseData] = {
    doRequest(HttpRequest(method = HttpMethods.GET, uri = url, headers, FormData(query).toEntity), discardEntity, timeout)
  }

  private def doRequest(req: HttpRequest, discardEntity: Boolean = false, timeout: Int = 0): Future[HTTPSimpleClientResponseData] = {
    val responseData = Promise[HTTPSimpleClientResponseData]()
    makeRequest(req)
      .onComplete {
        case Success(res) => {
          if (timeout > 0) {
            res.toStrict(timeout.seconds).map { res =>
              responseData.completeWith(consumeEntity(res, discardEntity))
            }
          } else
            responseData.completeWith(consumeEntity(res, discardEntity))
        }
        case Failure(fail) => {
          shutdown()
          responseData.failure(fail)
        }
      }
    responseData.future
  }

  private def makeRequest(req: HttpRequest): Future[HttpResponse] = {
    val h = http.getOrElse(start())
    h.singleRequest(req)
  }

  private def consumeEntity(res: HttpResponse, discardEntity: Boolean): Future[HTTPSimpleClientResponseData] = {
    val resDecoded = decodeEntity(res)
    if (discardEntity)
      getOnlyMetas(resDecoded)
    else
      getAllResponse(resDecoded)
  }

  private def getOnlyMetas(res: HttpResponse): Future[HTTPSimpleClientResponseData] = {
    res.discardEntityBytes()
    Future(HTTPSimpleClientResponseData(makeMetas(res), HTTPSimpleClientResponseDataContent(None, None)))
  }

  private def makeMetas(res: HttpResponse): HTTPSimpleClientResponseMetas = HTTPSimpleClientResponseMetas(
    res.headers,
    res.entity.httpEntity.contentType,
    res.protocol,
    res.status
  )

  private def getAllResponse(res: HttpResponse): Future[HTTPSimpleClientResponseData] = {
    val metas = makeMetas(res)
    res.entity.dataBytes
      .runFold(ByteString.empty)(_ ++ _)
      .map { body =>
        val isBinary = metas.contentType.binary
        if (isBinary)
          HTTPSimpleClientResponseData(metas,
            HTTPSimpleClientResponseDataContent(None, Some(body.toByteBuffer)))
        else
          HTTPSimpleClientResponseData(metas,
            HTTPSimpleClientResponseDataContent(Some(body.utf8String), None))
      }
  }

  private def decodeEntity(res: HttpResponse): HttpResponse = {
    res.encoding match {
      case HttpEncodings.gzip => Gzip.decode(res)
      case HttpEncodings.deflate => Deflate.decode(res)
      case HttpEncodings.identity => res
      case _ => res //
    }
  }
}

