/*
 * Copyright (C) 2018 Massimo Maria Ghisalberti <zairik@gmail.com>
 *
 * The contents of this file are subject to the GNU General Public License
 * Version 3 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.gnu.org/copyleft/gpl.html
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 */

package org.pragmas.http

import akka.actor.ActorSystem
import akka.event.LoggingAdapter
import akka.http.scaladsl.{Http, HttpExt}
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

trait HTTPSimple {

  protected val systemName = "HTTPSimple"
  protected val shutdownAwaitTimeout = 3.seconds
  protected implicit var system: Option[ActorSystem] = None
  protected implicit var materializer: ActorMaterializer = _
  protected implicit var executionContext: ExecutionContextExecutor = _

  protected var log: LoggingAdapter = _
  protected var http: Option[HttpExt] = None

  def shutdown(async: Boolean = false): Unit = {
    if (async) shutdownAsync()
    else shutdownAwait()
  }

  private def shutdownAwait(): Unit = {
    system match {
      case Some(s) => {
        Await.result(Future {
          http.get.shutdownAllConnectionPools()
          system.get.terminate()
        }, shutdownAwaitTimeout)
        system = None
        http = None
      }
      case _ => {
        //noop
      }
    }
  }

  private def shutdownAsync(): Unit = {
    system match {
      case Some(s) =>
        http.get.shutdownAllConnectionPools()
          .onComplete {
            case Success(_) => {
              s.terminate().onComplete {
                case Success(_) => {
                  system = None
                  http = None
                }
                case Failure(_) => sys.error(s"Error on terminate $systemName")
              }
            }
            case Failure(_) => sys.error(s"Error on terminate  $systemName")
          }
      case _ => {} //noop
    }
  }

  def start(): HttpExt = {
    http match {
      case Some(h) => h
      case _ => {
        system = Some(actorSystem)
        materializer = ActorMaterializer()(system.get)
        executionContext = system.get.dispatcher
        log = system.get.log
        http = Some(Http()(system.get))
        http.get
      }
    }
  }

  protected def actorSystem: ActorSystem = {
    system.getOrElse {
      val config = ConfigFactory.load()
      ActorSystem(systemName,
        config.getConfig(systemName).withFallback(config))
    }
  }

}
